# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
interests = ["footbal", "dance", "bouldering", "singing", "video games"]
interests.map{|i| Interest.create name: i}

languages = JSON.parse(File.read("app/assets/languages.json"))

languages.map{|l| Language.create name: l["language"].downcase, code: l["code"] }