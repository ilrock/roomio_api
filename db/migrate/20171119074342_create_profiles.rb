class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.text :bio

      t.timestamps
    end

    add_column :users, :profile_id, :integer
  end
end
