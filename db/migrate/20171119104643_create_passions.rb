class CreatePassions < ActiveRecord::Migration[5.1]
  def change
    create_table :passions do |t|
      t.references :interest, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
