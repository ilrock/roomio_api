class ChangeUserToProfileInPassion < ActiveRecord::Migration[5.1]
  def change
    remove_column :passions, :user_id, :integer
    add_column :passions, :profile_id, :integer
  end
end
