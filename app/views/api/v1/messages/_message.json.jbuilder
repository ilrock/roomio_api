json.extract! message, :content
if message.user
    json.user message.user, partial: '/api/v1/users/user', as: :user
end