json.extract! user, :id, :name, :username, :email

if user.profile
        json.profile user.profile, partial: '/api/v1/profiles/profile', as: :profile
        # json.name user.profile.name
        # json.bio user.profile.bio
        # json.picture user.profile.picture.url
end