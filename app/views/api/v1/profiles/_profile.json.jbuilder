# json.extract! profile, :name, :bio, :picture
json.profile
    json.name profile.name
    json.bio profile.bio
    json.picture profile.picture.url
    
    if profile.user.username
        json.username profile.user.username
    end
    
    if profile.interests
        json.interests profile.interests do |interest|
            json.(interest, :name)
        end
    end

    if profile.languages
        json.languages profile.languages do |language|
            json.(language, :name)
        end
    end