json.extract! chat, :id
json.other_user @other_user, partial: '/api/v1/users/user', as: :user

if chat.messages
    json.messages chat.messages do |message|
        json.(message, :content)
        if message.user
            json.user message.user, partial: '/api/v1/users/user', as: :user
        end
    end
end