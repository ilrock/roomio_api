class User < ApplicationRecord
  has_secure_password

  has_one :profile
  has_many :messages, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  has_many :chats, through: :subscriptions
  has_many :notifications
  
  after_create :create_profile

  def create_profile
    self.profile = Profile.create
  end

  def existing_chats_users
    existing_chat_users = []
    self.chats.each do |chat|
      existing_chat_users.concat(chat.subscriptions.where.not(user_id: self.id).map {|subscription| subscription.user})
    end
    existing_chat_users.uniq
  end

  def picture_url(profile)
    return profile.picture.url
  end
end
