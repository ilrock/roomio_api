class Profile < ApplicationRecord
  belongs_to :user, optional: true

  has_many :passions
  has_many :interests, -> { distinct }, through: :passions

  has_many :profile_languages
  has_many :languages, -> { distinct }, through: :profile_languages
  
  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png" 
  validates_attachment :picture
  do_not_validate_attachment_file_type :picture

  after_update :assign_username

  def picture_url(profile)
    return profile.picture.url
  end

  def assign_username
    if self.user && self.name && !self.user.username
      self.user.update_attributes username: self.name.gsub(/\s+/, "").downcase + rand(1..100).to_s
    end
  end
end
