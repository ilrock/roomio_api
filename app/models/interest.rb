class Interest < ApplicationRecord
    has_many :passions
    has_many :profiles, -> { distinct }, through: :passions

    validates :name, uniqueness: { case_sensitive: false }

end
