class Message < ApplicationRecord
  belongs_to :user
  belongs_to :chat

  after_create :broadcast_on_channel

  def broadcast_on_channel
    ActionCable.server.broadcast("messages_#{self.chat_id}",
      message: self.content,
      user:{
        name: self.user.profile.name,
        username: self.user.username,
        profile: { picture: self.user.profile.picture }
      }
    )
  end
end
