module Api
  module V1
    class InterestsController < ApplicationController
      def index
        if params[:query]
          @interests = Interest.where("name like ?", "%#{params[:query]}%")
        else
          @interests = Interest.all
        end
      end
    end
  end
end
