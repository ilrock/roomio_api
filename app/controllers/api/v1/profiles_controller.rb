module Api
  module V1
    class ProfilesController < ApplicationController
      before_action :authenticate_user
      def index
        # @profiles = Profile.all
        @profiles = Profile.where.not(id: current_user.profile.id)
      end

      def update
        @profile = current_user.profile
        if params[:interests] && params[:interests].length != 0
          params[:interests].each do |interest|
            if Interest.find_by_name interest[:name]
              @profile.interests.push Interest.find_by_name interest[:name]
            else
              @profile.interests.push Interest.create name: interest[:name]
            end
          end
        end

        if params[:languages] && params[:languages].length != 0
          params[:languages].each do |language|
            @profile.languages.push Language.find_by_name language[:name]
          end
        end

        if @profile.update(profile_params)
          render :show, status: :ok
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      def remove_interest
        if current_user
          @profile = current_user.profile
          if params[:deleted_interest]
            interest = Interest.find_by_name params[:deleted_interest][:name]
            @profile.passions.where(interest_id: interest.id).first.destroy
          end
          render :show, status: :ok
        end
      end

      def remove_language
        if current_user
          @profile = current_user.profile
          if params[:deleted_language]
            language = Language.find_by_name params[:deleted_language][:name]
            @profile.profile_languages.where(language_id: language.id).first.destroy
          end
          render :show, status: :ok
        end
      end

      private

      def profile_params 
        params.permit(:name, :bio, :picture)
      end
    end
  end
end
