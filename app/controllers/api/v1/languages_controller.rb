module Api
    module V1
      class LanguagesController < ApplicationController
        def index
          if params[:query]
            @languages = Language.where("name like ?", "%#{params[:query]}%")
          else
            @languages = Language.all
          end
        end
      end
    end
  end
  