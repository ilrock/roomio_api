require 'securerandom'
module Api
    module V1
        class ChatsController < ApplicationController
            before_action :authenticate_user
            
            def index
                chats = current_user.chats
                @existing_chats_users = current_user.existing_chats_users
            end

            def create
                @other_user = User.find(params[:other_user])

                if Chat.includes(:users).where(:users => {id: params[:other_user]}) != []
                    @chat = Chat.includes(:users).where(:users => {id: params[:other_user]}).first
                else
                    @chat = Chat.new(identifier: SecureRandom.hex)
                end

                if @chat.save
                    @chat.subscriptions.create(user_id: current_user.id)
                    @chat.subscriptions.create(user_id: @other_user.id)
                    render :show
                end
            end

            def show
                @other_user = User.find(params[:other_user])
                @chat = Chat.find_by(id: params[:id])
                @message = Message.new
            end

            private
            def find_chat(second_user)
                chats = current_user.chats
                chats.each do |chat|
                    chat.subscriptions.each do |s|
                        if s.user_id == second_user.id
                            return chat
                        end
                    end
                end
                nil
            end
        end
    end
end