Rails.application.routes.draw do
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :users, except: [:show]
      resources :chats, only: [:index, :show, :create]
      resources :interests
      resources :messages, only: [:create]
      resources :profiles, only: [:show, :index]
      put '/profile' => "profiles#update"
      put '/profile/remove_interest' => "profiles#remove_interest"
      put '/profile/remove_language' => "profiles#remove_language"
      get '/users/me' => 'users#me'
      get '/users/:username' => 'users#show'
      get '/languages' => 'languages#index'
      post 'user_token' => 'user_token#create'
    end
  end
end